# DVD Library

DVD library hosted on Google Cloud Free tier

# About project :
initialise sub-modules with : `git submodule update --init --recursive`
- reactjs-app : IHM project
- dvdfr-service : DVD EAN API provided bu DVDFR service
- dvdstore-service : DVD library provided, hosted on MySQL compute engine
- Authentication : provided by Auth0, but a legacy project exists with Spring OAuth2.

# Objectives

This project aims to test microservices on Google Cloud always free services such as AppEngine and Cloud Functions.
The functional goal is to host dvds library, and allow do share them with friends and family.

EAN samples : 
- 3607483160671
- 7321950586097
- 3333297192910
- 3344428015602
- 3459370501285

test user : 
* test@test.com / password

# TODO List
* Ajout d'une solution pour flasher le code barre depuis le mobule (barcode scanner javascript)
* Retrait de l'utilisation MySQL au profit du NoSQL GCP Firestore
* Mise en place d'une API Gateway Zuul
* Portage du service DVDFR vers une solution serverless (Google Cloud functions)
* Ajout de fonctionnalités de prêt à un tiers
* Ajout d'une page d'édition avec description et autres champs
* Ajout d'une fonction de "matching" via les APIs DVDFR et TMDB
* Ajout d'une page de saisie du titre si le code barre n'est pas trouvé sur dvdFR
